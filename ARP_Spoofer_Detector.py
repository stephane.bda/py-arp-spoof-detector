# -*- coding: utf-8 -*-
# !/usr/bin/env python

import scapy.all as scapy


def get_mac(ip):
    # Set Address Resolution Protocol & ip destination
    arp_request = scapy.ARP(pdst=ip)
    # Set & Send to broadcast mac adress
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    #  srp return 2 list : answered & unanswered  | timeout = don't wait
    #  answered = 0 | unanswered = 1 ; verbose=False to undisplay some information (nbre packet, ect)
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    # see the content with print(element[1].show()) then you can print only psrc, hwsrc, etc
    # see the ip adress (.psrc) of the response source & the mac adress (.hwsrc)
    return answered_list[0][1].hwsrc


def sniff(interface):
    # store=False = Don't store in memory | prn = for every packet sniff callback function process_sniffed_packet
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)


def process_sniffed_packet(packet):
    # Require scapy.layers http
    if packet.haslayer(scapy.ARP) and packet[scapy.ARP].op == 2:
        try:
            # real_mac = get_mac(ip)
            real_mac = get_mac(packet[scapy.ARP].psrc)
            response_mac = packet[scapy.ARP].hwsrc

            if real_mac != response_mac:
                print("[+] You are under Attack !")
        except IndexError:
            pass


sniff("eth0")
