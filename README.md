# Py ARP Spoof Detector

## Require for windows :
1. Install python3 : [python3](https://www.python.org/downloads/)
2. Install latest npcap : [npcap](https://nmap.org/npcap/)
3. install latest scapy : [scapy](https://github.com/secdev/scapy)
   * Case Python is in Path :
       * Enter in command prompt with `cmd` then `python -m pip install scapy`
   * Case Python is not in Path :
       * Enter in command prompt with `cmd` then `C:\Your\Path\To\Python\python.exe -m pip install scapy`

## Linux :
1. No need to install Python, most linux distrib come with python interpreter
2. Install Scapy : `sudo pip install scapy`
